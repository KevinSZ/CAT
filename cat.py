import shutil
import os
import sys
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('input', nargs='*', metavar='c', type=str, help='input files name')
parser.add_argument('-n','--number', help='number all output lines', action='store_true')
parser.add_argument('-s','--squeeze-blank', help='suppress repeated empty output lines', action='store_true')
parser.add_argument('-E','--show-ends', help='display $ at end of each line', action='store_true')
args = parser.parse_args()
#print(args)

#print([str.upper(my_string) for my_string in args.input])

line_counter=0
for my_string in args.input:
	with open(my_string, 'r') as file:
		content = file.read()
		lines = content.split('\n')
		for line in lines:
			if args.squeeze_blank == True and line == "":
				print(end="")
			else:
				if args.number == True:
					print(line_counter, end="")
				print(line, end="")
				if args.show_ends == True:
					print("$", end="")
				print(" ")
				line_counter += 1
